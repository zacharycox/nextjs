import React from 'react'
import { observable, toJS, computed } from 'mobx'
import { observer } from 'mobx-react'
import fetch from 'isomorphic-fetch'
import * as moment from 'moment'

import Input from './input.js'
import Checkbox from './checkbox.js'

// import './style.scss'

@observer
// export default class ContactForm extends React.Component<{}> {
export default class ContactForm extends React.Component {
  // @observable formData: Map<string, string | boolean> = new Map()
  @observable formData = new Map()
  // @observable termsMissing: boolean
  @observable termsMissing
  // @observable submitState: 'open' | 'submitting' | 'success' | 'error' = 'open'
  @observable submitState = 'open'

  // @computed get dobInvalid (): boolean {
  @computed get dobInvalid() {
    if (!this.formData.has('dob')) return false
    // const b = moment(this.formData.get('dob') as string)
    const b = moment(this.formData.get('dob'))
    const n = moment()
    return n.diff(b, 'years') < 21
  }

  // @computed get notInUs (): boolean {
  @computed get notInUs() {
    return this.formData.get('state') === 'other'
  }

  // onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  handleChange = (e) => {
    if (e.target.name === 'dob') {
      this.formData.set(e.target.name, moment(e.target.value).format('YYYY-MM-DD'))
    } else {
      this.formData.set(e.target.name, e.target.type === 'checkbox' ? e.target.checked : e.target.value)
    }
  }

  // onSubmit = (e: React.FormEvent): void => {
  handleSubmit = (e) => {
    e.preventDefault()

    if (this.notInUs || this.dobInvalid) {
      return
    }

    this.termsMissing = false
    if (!this.formData.has('terms') || !this.formData.get('terms')) {
      this.termsMissing = true
      return
    }

    this.submitState = 'submitting'

    console.log('data: ', formData)
    this.callCidb(toJS(this.formData))
  }

  // fetchPPVersionOnLoad = (): void => {
  fetchPPVersionOnLoad = () => {
    try {
      // const ppVer = (window as any).dg_footer_configurations.hg_PP_Version
      const ppVer = window.dg_footer_configurations.hg_PP_Version
      if (ppVer) {
        this.formData.set('ppVersion', ppVer)
      } else {
        throw new Error('Cannot get Privacy Policy version')
      }
    } catch (err) {
      console.warn(err)
    }
  }

  // async callCidb (data): Promise<void> {
  async callCidb (data) {
    try {
      const res = await fetch('/api/cidb', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify(data)
      }).then(res => res.json())
      if (res.success) {
        this.submitState = 'success'
      } else {
        this.submitState = 'error'
      }
    } catch (error) {
      this.submitState = 'error'
    }
  }

  // componentDidMount () {
  //   this.fetchPPVersionOnLoad()
  // }

  render () {
    return (
      <section className='register' id='register'>
        <div className='container'>
          <h2 className='section-header'>Be notified of release</h2>
          <h3 className='section-subheader'>Be first to know when the Bourbon is available for purchase.</h3>

          {this.submitState === 'open' &&
          <form className='middle-content' onSubmit={this.handleSubmit}>
            <div className='row'>
              <div className='col-12 col-md-6 col-lg-4'>
                <Input type='text' name='firstName' onChange={this.handleChange} maxLength={30} required>
                  First Name
                </Input>
              </div>
              <div className='col-12 col-md-6 col-lg-4'>
                <Input type='text' name='lastName' onChange={this.handleChange} maxLength={30} decor={2} required>
                  Last Name
                </Input>
              </div>
              <div className='col-12 col-md-6 col-lg-4'>
                <Input type='text' name='email' onChange={this.handleChange} maxLength={30} required>
                  Email Address
                </Input>
              </div>
              <div className='col-12 col-md-6 col-lg-4'>
                <Input type='date' name='dob' onChange={this.handleChange} placeholder='MM/DD/YYYY' withLabel required>
                  DOB&nbsp;
                </Input>
              </div>
              <div className='col-12 col-md-6 col-lg-4'>
                <Input type='select' name='state' onChange={this.handleChange} placeholder='Choose' withLabel required>
                  State&nbsp;
                </Input>
              </div>
              <div className='col-12 col-md-6 col-lg-4'>
                <button type='submit' className='btn btn-red'>
                  <span>Get Updates</span>
                </button>
              </div>
            </div>
            <div className='checkboxes'>
              <Checkbox name='news' onChange={this.handleChange}>
                Would you like to receive special offers and news about Spirits of the Apocalypse by Email?
              </Checkbox>
              <Checkbox name='terms' onChange={this.handleChange}>
                I accept the terms of use and privacy and cookie notice*
              </Checkbox>
              {this.termsMissing &&
              <div className='invalid-feedback text-center'>Please accept the terms of use and privacy and cookie notice!</div>
              }
              {this.dobInvalid &&
              <div className='invalid-feedback text-center'>You need to be of a legal drinking age to register!</div>
              }
              {this.notInUs &&
              <div className='invalid-feedback text-center'>E-Mail updates are only available to users located in the United States.</div>
              }
            </div>
          </form>
          }

          {this.submitState === 'submitting' &&
          <div className='middle-content text-center'>Submitting...</div>
          }

          {this.submitState === 'success' &&
          <div className='middle-content text-center'>Thank you for your submission!</div>
          }

          {this.submitState === 'error' &&
          <div className='middle-content text-center'>An error happened.</div>
          }

          <div className='row justify-content-center'>
            <div className='col-sm-8'>
              <p className='legal text-center'>
                Consequat nulla ut ipsum incididunt enim qui sit reprehenderit. Non sint incididunt eu non excepteur cillum et cillum enim dolor aute ullamco. Pariatur enim anim aute aliquip pariatur nisi officia.
              </p>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
