import React from 'react';
import Navitem from './navitem.js'

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.navitems = this.props.navitems;
  }

  render() {
    return (
      <nav className={'nav'}>
        {this.navitems.map((item, i) => {return (
            <Navitem text={item["text"]} url={item["url"]} key={i} />
          );}
        )}
      </nav>
    )
  }
}

export default Navbar;
