import React from 'react';
import Link from "next/link";

class Navitem extends React.Component {
  render() {
    return (
      <Link href={ this.props.url }>
        <a className={'nav__item'}>
          <div className={'nav__item-text'}>{ this.props.text }</div>
        </a>
      </Link>
    )
  }
}

export default Navitem;
