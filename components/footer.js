import React from 'react'
import { inject, observer } from 'mobx-react'

// import './style.scss'

// @inject('store')
@observer
// export default class Footer extends React.Component<{}> {
export default class Footer extends React.Component {
  render () {
    return (
      <footer className={'footer'} id={'footer'}>
        <div className={'container'}>
          <div className='row'>
            <div className='col-6 col-md-2'>
              <img className='img-fluid' src='/static/images/logo-spirit.png' alt='Spirits of the Apocalypse' />
            </div>
            <div className='col-md-7'>
              <div id='footer' />
            </div>
            <div className='col-md-3 text-right'>
              <a href='#'className='social-icon'>
                <img src='/static/images/icon-facebook.svg' alt='Share on Facebook' />
              </a>
              <a href='#'className='social-icon'>
                <img src='/static/images/icon-twitter.svg' alt='Share on Twitter' />
              </a>
            </div>
          </div>
          <p className='legal'>Incididunt excepteur laborum reprehenderit nostrud </p>
          <p className='legal'>Incididunt excepteur laborum reprehenderit nostrud laborum laboris mollit amet mollit ad cupidatat irure excepteur. </p>
        </div>
      </footer>
    )
  }
}
