import React from 'react';

function Meta() {
  return(
    <>
      <meta name={'viewport'} content={'width=device-width, initial-scale=1'} />
      <meta charSet={'utf-8'} />
    </>
  );
}

export default Meta;
