import React from 'react'
import { inject, observer } from 'mobx-react'

// import './style.scss'

// @inject('store')
@observer
// export default class Footer extends React.Component<{}> {
export default class FooterDG extends React.Component {
  render () {
    return (
      <>
        <footer className={'footer'} id={'footer'}></footer>
        <script type="text/javascript" src="//footer.diageohorizon.com/dfs/master.js"></script>
      </>
    )
  }
}
