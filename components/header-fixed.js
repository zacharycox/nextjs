// Fixed Header component
import React from 'react';
import Link from 'next/link';
import Logo from '../assets/images/global/logo.jpg';
import Navbar from './navbar';
let navitems = require('../data/nav.json');


class HeaderFixed extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    // let navitems = [ 'text', 'page2', 'url', './page2'];

    // let navitems = [
    //   {
    //     text: 'page2',
    //     url: './page2',
    //   },
    //   {
    //     text: 'page3',
    //     url: './page3',
    //   },
    //   {
    //     text: 'page4',
    //     url: './page4',
    //   },
    // ];


    return (
      <>
        <header id={'headerFixed'} className={'hdr-fixed'}>
          <div className={'container'}>
            <div className={'logo'}>
              <Link href={'/'}>
                  <div className={'logo__img-wrap'}>
                    <img className={'logo__img'} src={ Logo } alt={'Thorium Digital'} title={'Thorium Digital'} />
                  </div>
              </Link>
            </div>
            <Navbar navitems={ navitems } />
          </div>
        </header>
      </>
    );
  }
}

export default HeaderFixed;

