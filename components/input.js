import React from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'

// import './style.scss'

@observer
// export default class Input extends React.Component<{
//   decor?: number
//   type: string
//   name: string
//   onChange: (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void
//   placeholder?: string
//   withLabel?: boolean
//   required?: boolean
//   maxLength?: number
// }> {
export default class Input extends React.Component {
  // @observable value: string
  @observable value

  // onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
  onChange = (e) => {
    this.value = e.target.value
    this.props.onChange(e)
  }

  render () {
    const { children, type, name, withLabel, required, decor, placeholder, maxLength } = this.props
    return (
      <div className={`custom-input ${withLabel ? 'with-label' : 'with-placeholder'} decor-${decor || 1}`}>
        <div className='content'>
          {(!this.value || withLabel) &&
          <div className='label'>
            {children}
            {!withLabel && required &&
            <span className='star'>*</span>
            }
          </div>
          }
          {type === 'select'
            ? <select name={name} onChange={this.onChange} required={required} defaultValue={''}>
              <option value='' disabled>Choose</option>
              <option value='AL'>ALABAMA</option>
              <option value='AK'>ALASKA</option>
              <option value='AZ'>ARIZONA</option>
              <option value='AR'>ARKANSAS</option>
              <option value='CA'>CALIFORNIA</option>
              <option value='CO'>COLORADO</option>
              <option value='CT'>CONNECTICUT</option>
              <option value='DE'>DELAWARE</option>
              <option value='FL'>FLORIDA</option>
              <option value='GA'>GEORGIA</option>
              <option value='HI'>HAWAII</option>
              <option value='ID'>IDAHO</option>
              <option value='IL'>ILLINOIS</option>
              <option value='IN'>INDIANA</option>
              <option value='IA'>IOWA</option>
              <option value='KS'>KANSAS</option>
              <option value='KY'>KENTUCKY</option>
              <option value='LA'>LOUISIANA</option>
              <option value='ME'>MAINE</option>
              <option value='MD'>MARYLAND</option>
              <option value='MA'>MASSACHUSETTS</option>
              <option value='MI'>MICHIGAN</option>
              <option value='MN'>MINNESOTA</option>
              <option value='MS'>MISSISSIPPI</option>
              <option value='MO'>MISSOURI</option>
              <option value='MT'>MONTANA</option>
              <option value='NE'>NEBRASKA</option>
              <option value='NV'>NEVADA</option>
              <option value='NH'>NEW HAMPSHIRE</option>
              <option value='NJ'>NEW JERSEY</option>
              <option value='NM'>NEW MEXICO</option>
              <option value='NY'>NEW YORK</option>
              <option value='NC'>NORTH CAROLINA</option>
              <option value='ND'>NORTH DAKOTA</option>
              <option value='OH'>OHIO</option>
              <option value='OK'>OKLAHOMA</option>
              <option value='OR'>OREGON</option>
              <option value='PA'>PENNSYLVANIA</option>
              <option value='RI'>RHODE ISLAND</option>
              <option value='SC'>SOUTH CAROLINA</option>
              <option value='SD'>SOUTH DAKOTA</option>
              <option value='TN'>TENNESSEE</option>
              <option value='TX'>TEXAS</option>
              <option value='UT'>UTAH</option>
              <option value='VT'>VERMONT</option>
              <option value='VA'>VIRGINIA</option>
              <option value='WA'>WASHINGTON</option>
              <option value='WV'>WEST VIRGINIA</option>
              <option value='WI'>WISCONSIN</option>
              <option value='WY'>WYOMING</option>
              <option value='other'>Other</option>
            </select>
            : <input type={type} name={name} onChange={this.onChange} required={required} placeholder={placeholder} maxLength={maxLength} />
          }
          {withLabel && required &&
          <span className='star'>*</span>
          }
        </div>
      </div>
    )
  }
}
