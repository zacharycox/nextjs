import React from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'

// import './style.scss'

@observer
// export default class Checkbox extends React.Component<{
//   name: string
//   onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
//   required?: boolean
// }> {
export default class Checkbox extends React.Component {
  // @observable value: string
  @observable value

  // onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  onChange = (e) => {
    this.value = e.target.value
    this.props.onChange(e)
  }

  render () {
    const { children, name, required } = this.props
    return (
      <div className={`custom-checkbox`}>
        <div className='content'>
          <input type='checkbox' name={name} id={name} onChange={this.onChange} required={required} />
          <label htmlFor={name}>
            <div className='custom-box'>
              <div className='bg' />
              <div className='tick' />
            </div>
            <span>
              {children}
            </span>
          </label>
        </div>
      </div>
    )
  }
}
