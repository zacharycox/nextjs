import React from 'react';
import Head from 'next/head';
import Meta from '../components/meta';
import '../styles/main.scss';
import HeaderFixed from '../components/header-fixed';
// import Footer from '../components/footer';
import FooterDG from '../components/footer-dg';


export default ({ children }) => (
  <>
    <Head>
      <title>Next Boilerplate</title>
      <Meta />
      <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossOrigin="anonymous"></script>
    </Head>
    <HeaderFixed />
    { children }
    {/*<Footer />*/}
    <FooterDG />
  </>
)
