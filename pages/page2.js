import React from 'react';
import Layout from '../layouts/default.js';
import Link from 'next/link';
// import ContactForm from '../components/z-form'
import ContactForm from '../components/contact-form'

function Page2() {
  return (
    <Layout>
      <main className={'page2'}>
        <section className={'page2__hero'}>
          <div className={'container'}>
            <h1 className={'hero__hdg'}>this be page2 hero</h1>
            <div><Link href={'/'}><a>home</a></Link></div>
          </div>
        </section>
        <section className={'page2__section2'}>
          <div className={'container'}>
            <h1 className={'section__hdg'}>this be page2 section</h1>
            <ContactForm />
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Page2;
