import React from 'react';
import Layout from '../layouts/default.js';
import '../styles/main.scss';
import Link from 'next/link';

function Home() {
  return (
    <Layout>
      <main className={'home'}>
        <section>
          <div className={'container'}>
          <div className={'home__hero'}>
            <h1 className={'hero__hdg'}>Welcome to Thorium's NextJS Boilerplate!</h1>
            <div><Link href={'/page2'}><a>page2</a></Link></div>
          </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Home;
