// next.config.js
const path = require('path');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');

module.exports = withImages(withSass({
  sassLoaderOptions: {
    includePath: ["styles", "styles/pages"],
    sourceComments: true,
    lineNumber: true
  }
}));

