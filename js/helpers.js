// Helper/utility/convenience functions
// todo: create utility class (see: https://gualtierotesta.wordpress.com/2013/06/25/utility-functions-in-javascript/)

export function executeFunctionByName(functionName, context /*, args */) {
  let args = Array.prototype.slice.call(arguments, 2);
  let namespaces = functionName.split(".");
  let func = namespaces.pop();
  for(let i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(context, args);
}
